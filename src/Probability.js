export default class Probability{

  static get ABSOLUTE_PROBABILITY(){
    return 1;
  }

  constructor(value){
    this.value = value;
  }

  compare(probability){
    if(this.value > probability.value){
      return 1;
    }
    if(this.value < probability.value){
      return -1;
    }
    return 0;
  }

  and(probability){
    return new Probability(this.value * probability.value);
  }

  not(){
    return new Probability(Probability.ABSOLUTE_PROBABILITY-this.value);
  }

  equals(anotherProbability){
    return this.value === anotherProbability.value;
  }
}
