import Probability from "../src/Probability"

describe("Probability", () => {

  describe("Equals", () => {
    test("should return true when probability is 0.4 and anotherProbability is"
        + " 0.4", () => {
      const probability = new Probability(0.4);
      const anotherProbability = new Probability(0.4);

      expect(probability.equals(anotherProbability)).toEqual(true);

    })

    test("should return false when probability is 0.4 and anotherProbability is"
        + " 0.3", () => {
      const probability = new Probability(0.4);
      const anotherProbability = new Probability(0.3);

      expect(probability).not.toEqual(anotherProbability);

    })
  })

  describe("Compare", () => {

    test(
        "should return 1 when probability is 0.4 and anotherProbability is 0.3",
        () => {
          const probability = new Probability(0.4);
          const anotherProbability = new Probability(0.3);

          const actualResult = probability.compare(anotherProbability);

          expect(actualResult).toEqual(1);
        })

    test("should return -1 when probability is 0.3 and anotherProbability is"
        + " 0.4", () => {
      const probability = new Probability(0.3);
      const anotherProbability = new Probability(0.4);

      const actualResult = probability.compare(anotherProbability);

      expect(actualResult).toEqual(-1);
    })

    test(
        "should return 0 when probability is 0.3 and anotherProbability is 0.3",
        () => {
          const probability = new Probability(0.3);
          const anotherProbability = new Probability(0.3);

          const actualResult = probability.compare(anotherProbability);

          expect(actualResult).toEqual(0);
        })
  })

  describe("Operations", () => {

    test(
        "and should return product of probabilities when probability is 0.4 and"
        + " anotherProbability is 0.3", () => {
          const probability = new Probability(0.4);
          const anotherProbability = new Probability(0.3);
          const expectedProbability = new Probability(0.12);

          const actualProbability = probability.and(anotherProbability);

          expect(actualProbability).toEqual(expectedProbability);
        })

    test("not should return not of probability when probability is 0.4", () => {
      const probability = new Probability(0.4);
      const expectedProbability = new Probability(0.6);

      const actualProbability = probability.not();

      expect(actualProbability).toEqual(expectedProbability);
    })
  })
})
